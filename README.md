# DNAIQ-api : Client Reference Implementation

This repo is a reference implementation of the client side of athe DNAiQ API (for results).

This has been created using PHP and the slimPHP 4 framework. This was used to test the API Server interaction.

Clients are welcome to fork this Repo and adapt to their needs. 

NOTE: No warranty is expressed or impled and is offered on an "as is" basis.

## API Documentation 

[Can be found here.](./API.documentation.md)

## Initialise

**Pre-requisites**
- PHP 7.2 or higher
- Composer
- Docker Host

**Then**   
Clone this repo and navigate tp project root and:   
```bash
composer install
```   
```bash
docker network create api-net
```   
rename and update /src/server/.env.example   

## Commands

The following :
```bash
docker-compose up
``` 
- starts a docker container in dev mode
- expects posts on port 9000
- changes to `index.php` are reflected immedately
- important items are logged in colour to the console when running   

The following :
```bash
docker build . -f Dockerfile.prod -t api-client
```
- builds a docker image of the current project
- You will need to ensure either .env is in the build or provide `API_POST_TOKEN` and `API_GET_TOKEN` environment variables at docker run time.

The following :   
```bash
docker --name api-client run -p 9000:8080 --network api-net api-client
```   
- runs the previously built image   
- expects posts on port 9000
- other containers can connect directly via api-client:8080 IF those container waere also started with `--network api-net`
