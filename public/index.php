<?php
// phpinfo();
require_once __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/server/colors.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Factory\AppFactory;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../src/server/');
$dotenv->load();
$dotenv->required(['API_POST_TOKEN', 'API_GET_TOKEN']);

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
  $cc = new Colors();
  $rt = 'GET /';
  $logger = new Logger($rt);
  $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
  $logger->info($cc->apply($rt,'light_purple','black'));
  $logger->info($cc->apply("Hello World",'yellow','black'));
  // Hello World
  $response->getBody()->write(file_get_contents('./hello.html', true));
  return $response;
});

$app->post('/notify', function (Request $request, Response $response, $args) { 
  $cc = new Colors();
  $rt = 'POST /notify';
  $logger = new Logger($rt);
  $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
  $logger->info($cc->apply($rt,'light_purple','black'));     

  $data = json_decode($request->getBody());

  $logger->info($cc->apply("Run ID : " . $data->runID,'yellow','black'));

  // Reject POST with non-conforming Authorization Header
  if ($request->hasHeader('Authorization')) {
    $hdrArray = $request->getHeader('Authorization');
    $actual_post_token = str_replace("Bearer ","",$hdrArray[0]);
    $expected_post_token = $_ENV['API_POST_TOKEN'];

    $logger->info($cc->apply("token: ".$actual_post_token, 'yellow','black'));
    if ($expected_post_token !== $actual_post_token) {
      $logger->info($cc->apply("Invalid token: ".$actual_post_token, 'yellow','black'));
      return $response
              ->withHeader('Content-Type', 'application/json')
              ->withStatus(401); 
    }
  } else {
    $logger->info($cc->apply("NO Authorisation Header",'yellow','black'));
    return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(401); 
  }

  // Set up API call 
  $token = $_ENV['API_GET_TOKEN'];
  $url = $data->getURL;
  $curl = curl_init($url); // initialise cURL

  // Set up CURL options
  curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Connection: Keep-Alive',
        "Authorization: Bearer $token",
    )
  ));

  $logger->info($cc->apply("cURL-ing: " . $data->getURL,'yellow','black'));
  $curl_response = curl_exec($curl); // Execute API via CURL
  if ($curl_response === false) {

    $info = curl_getinfo($curl);
    $logger->info($cc->apply("Bad URL >>> " . $info['url'],'yellow','black'));
    curl_close($curl);
    // $payload->url = $info['url'];
    $payload->url = $data->getURL;
    $response->getBody()->write(json_encode($payload));
    return $response
              ->withHeader('Content-Type', 'application/json')
              ->withStatus(421);

  } else  {
    
    $http_resp = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $data = json_decode($curl_response, true);

    // IF no error code
    if ($http_resp == 201) {
      $logger->info($cc->apply("Data returned-".$http_resp,'yellow','black'));
      $logger->info($cc->apply(substr($curl_response,0,256)." ||| TRUNCATED to first 256 bytes",'light_green','black'));
      $response->getBody()->write(json_encode($data));
      return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200); 
    } else {
      $logger->info($cc->apply("Error-".$http_resp,'yellow','black'));
      $response->getBody()->write($curl_response);
      return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($http_resp); 
    }
  }

});

$app->any('/{any:.*}', function (Request $request, Response $response, $args) {
  $cc = new Colors();
  $rt = '404';
  $logger = new Logger($rt);
  $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));
  $logger->info($cc->apply($rt,'light_purple','black'));

  $uri = $request->getUri();
  $logger->info($cc->apply($request->getMethod() . ' ' . $uri->getPath(),'yellow','black'));
  $response->getBody()->write('404 not found');
  return $response
          ->withStatus(404);
});

$app->run();
?>